const express = require('express'),
uploadController = require('../controllers/upload'),
passport = require('../security/passport');

const uploadRoutes = express.Router();
const multiparty = require('connect-multiparty');

const Multipartymiddleware = multiparty({uploadDir: './uploads'});

 
// /api/files/createTable
uploadRoutes.post('/createTable', passport.requireAuth, uploadController.createTable);

// /api/files/upload-single
uploadRoutes.post('/upload-single', passport.requireAuth, uploadController.uploadSingleFile);

// /api/files/upload-multiple
uploadRoutes.post('/upload-multiple', passport.requireAuth, uploadController.uploadMultipleFile);
 
// /api/files/:{category} 
uploadRoutes.get('/:category', uploadController.getFilesByCategory);
 
// /api/files/
uploadRoutes.get('/', uploadController.getAllFiles);

// /api/files/name/:{name}
uploadRoutes.get('/name/:name', uploadController.getFileByName);

// /api/files/uploads
uploadRoutes.post('/uploads', uploadController.upload);

// /api/files/uploadFileBase64
uploadRoutes.post('/uploadFileBase64', passport.requireAuth, uploadController.uploadFileBase64);

// /api/files/delete/:{id}
uploadRoutes.delete('/delete/:id', passport.requireAuth, uploadController.deleteFileById);




module.exports = uploadRoutes; 