const express = require('express'),
    clubsController = require('../controllers/clubs'),
    passport = require('../security/passport');
const clubRouter = express.Router();

// api/club/create_table
clubRouter.post('/create_table', passport.requireAuth, clubsController.createTable);

//api/club/create_club
clubRouter.post('/create_club', passport.requireAuth, clubsController.createClub);

//api/club/:id
clubRouter.delete('/:id', passport.requireAuth, clubsController.deleteClub);

//api/club/:id
clubRouter.get('/:id', clubsController.getClubById);

//api/club/
clubRouter.get('/', clubsController.getAllClubs);

module.exports = clubRouter