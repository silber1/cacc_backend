## 📑 Documentation:
    Project Back End 
    -  University of Delaware, FALL 2020, CISC475 Advanced Software Engineering, Group 9 Final Project. 
    -  Project: re-build Chinese American Community Center (CACC) Website.
    -  Client: Chinese American Community Center (CACC), Naxin Cai. 
    -  Development Group Member: Yixiong Wu, Letian Zhang, Sicheng Tian, Long Zheng, Xing Gao. 
    -  Instructor: Greg Silber, Teaching Assistant: Vinit Singh. 

<pre>Development tool: 
    Front End: Angular 
    Back End: Node.js 
    DateBase: MySQL 
</pre>

## 🚀 Installation:
For the Back end, we used Node.js. Therefore, for keeping working on the project, you should install the Node.js first. <br/>
<a href="https://nodejs.org/en/download/">For more information about Node.js, Click here &nbsp;&nbsp;▶</a>

#### 1. Clone the project from GitHub
```sh
git clone https://gitlab.com/cisc475group/cacc_backend.git
```

#### 2. At cacc_backend/ directory, install the node_module
```sh
npm install
```

#### 3. Run the server
```sh
npm start
```

## ✍ General Structure and Information:
- app.js : general set up of the server and initializing router
- dbService.js : data base configuration
- mailService.js : mail service configuration
- routes/ : write XXX API's route in XXX.js and place it under routes/
- controllers/ : write XXX API's function in XXX.js and place it under controller/
- security/ : under the security/, there are passport service(passport.js) for JWT and admin role authentication(adminRoleAuth.js)


##### API Request Body format
- /api/admin
    - / 
        ```sh
        Request Type: Get
        APIfunction: getAllAdmins
        Request Body: {}
        ```
    - /create_table
        ```sh
        Request Type: Get
        APIfunction: createTable
        Request Body: {}
        ```
    - /getAdminByEmail/:email
        ```sh
        Request Type: Get
        APIfunction: getAdminByEmail
        Request Body: {}
        ```
    - /:id
        ```sh
        Request Type: Delete
        APIfunction: deleteAdmins
        Request Body: {}
        ```
- /api/articles
    - /
        ```sh
        Request Type: Get
        APIfunction: getAllArticles
        Request Body: {}
        ```
    - /create_table
        ```sh
        Request Type: Post
        APIfunction: createTable
        Request Body: {}
        ```
    - /createArticle
        ```sh
        Request Type: Post
        APIfunction: createArticle
        Request Body: {title, content, category}
        ```
    - /category/:category
        ```sh
        Request Type: Get
        APIfunction: getArticlesByCategory
        Request Body: {}
        ```
    - /title/:title
        ```sh
        Request Type: Get
        APIfunction: getArticleByTitle
        Request Body: {}
        ```
    - /:id
        ```sh
        Request Type: Get
        APIfunction: getArticleById
        Request Body: {}
        ```
    - /delete/:id
        ```sh
        Request Type: Delete
        APIfunction: getAllAdmins
        Request Body: {}
        ```
    - /edit/:id
        ```sh
        Request Type: Put
        APIfunction: editArticleById
        Request Body: {content, title, category}
        ```
- /api/auth
    - /register
        ```sh
        Request Type: Post
        APIfunction: register
        Request Body: {email, password, confirm_password, role}
        ```
    - /login
        ```sh
        Request Type: Post
        APIfunction: login
        Request Body: {email, password}
        ```
- /api/club
    - /
        ```sh
        Request Type: Get
        APIfunction: getAllClubs
        Request Body: {}
        ```
    - /create_table
        ```sh
        Request Type: Post
        APIfunction: createTable
        Request Body: {}
        ```
    - /create_club
        ```sh
        Request Type: Post
        APIfunction: createClub
        Request Body: {engname, chiname}
        ```
    - /:id
        ```sh
        Request Type: Get
        APIfunction: getClubByID
        Request Body: {}
        ```
    - /:id
        ```sh
        Request Type: Delete
        APIfunction: deleteClubByID
        Request Body: {}
        ```
- /api/events
    - /
        ```sh
        Request Type: Get
        APIfunction: getAllEvents
        Request Body: {}
        ```
    - /create_table
        ```sh
        Request Type: Post
        APIfunction: createTable
        Request Body: {}
        ```
    - /createEvent
        ```sh
        Request Type: Post
        APIfunction: createEvent
        Request Body: {content, title, category, date, address, time, endTime}
        ```
    - /:event_id
        ```sh
        Request Type: Get
        APIfunction: getEvebtById
        Request Body: {}
        ```
    - /category/:category
        ```sh
        Request Type: Get
        APIfunction: getEventByCategory
        Request Body: {}
        ```
    - /title/:title
        ```sh
        Request Type: Get
        APIfunction: getEventByTitle
        Request Body: {}
        ```
    - /search/:title
        ```sh
        Request Type: Get
        APIfunction: searchEventByTitle
        Request Body: {}
        ```
    - /delete/:id
        ```sh
        Request Type: Delete
        APIfunction: deleteEventById
        Request Body: {}
        ```
    - /edit/:id
        ```sh
        Request Type: Put
        APIfunction: editEventById
        Request Body: {content, title, category, date, address, time, endTime}
        ```
- /api/headerMenu       (Note: this API is not in use)
    - /
        ```sh
        Request Type: Get
        APIfunction: getAllHeadermenu
        Request Body: {}
        ```
    - /create_table
        ```sh
        Request Type: Get
        APIfunction: createTable
        Request Body: {}
        ```
    - /add_menu
        ```sh
        Request Type: Post
        APIfunction: addMenu
        Request Body: {name, link, parent}
        ```
    - /update_by_id
        ```sh
        Request Type: Put
        APIfunction: updateById
        Request Body: {id, name, link, parent}
        ```
    - /delete_by_id
        ```sh
        Request Type: Delete
        APIfunction: deleteById
        Request Body: {id}
        ```
- /api/mailService
    - /send
        ```sh
        Request Type: Post
        APIfunction: sendEmail
        Request Body: {contactFormName, contactFormEmail, contactFormSubjects, contactFormMessage, contactFormCopy}
        ```
- /api/files
    - /
        ```sh
        Request Type: Get
        APIfunction: getAllFiles
        Request Body: {}
        ```
    - /createTable
        ```sh
        Request Type: Post
        APIfunction: createTable
        Request Body: {}
        ```
    - /upload-single
        ```sh
        Request Type: Post
        APIfunction: uploadSingleFile
        Request files
        ```
    - /upload-multiple
        ```sh
        Request Type: Post
        APIfunction: uploadMultipleFile
        Request files
        ```
    - /:category
        ```sh
        Request Type: Get
        APIfunction: getFilesByCategory
        Request Body: {}
        ```
    - /name/:name
        ```sh
        Request Type: Get
        APIfunction: getFileByName
        Request Body: {}
        ```
    - /uploads
        ```sh
        Request Type: Post
        APIfunction: upload
        Request files
        ```
    - /uploadFileBase64
        ```sh
        Request Type: Post
        APIfunction: uploadFileBase64
        Request Body: {name, category, base64}
        ```
    - /delete/:id
        ```sh
        Request Type: Delete
        APIfunction: deleteFileById
        Request Body: {}
        ```


## 🔎 Current Issue

##### file upload issue: After insert image in editor, delete it by backspace. The image would be upload to server, but it's path is not saved in database




## ⚠️ Notice
- All headerMenu Api are not used in client side
- Do not use the create table api if the table is already exist.


## 📝 License
<strong>© 2020 Chinese American Community Center</strong>
