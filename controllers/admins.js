const mysqlDb = require('../dbService');
const Joi = require('joi');



exports.createTable = function(req, res){
    let sql = "CREATE TABLE admins(id int AUTO_INCREMENT, email VARCHAR(255), password VARCHAR(255), role VARCHAR(255), PRIMARY KEY(id))";
    mysqlDb.query(sql, (err, result) => {
        if (err) throw err;
        console.log(result) ;
        res.status(200).send('admins table created..');
    });
}

exports.getAllAdmins = async function(req, res){
    try{
        const response = await new Promise((resolve, reject)=> {
            const query = "SELECT * FROM admins;";

            mysqlDb.query(query, (err, results) =>{
                if (err) reject(new Error(err.message));
                resolve(results);
            });
        })
        .then(data => res.json({data : data}))
        .catch(err => console.log(err));
        //res.send(response);
    } catch (error){
        console.log(error);
    }
}

exports.getAdminByEmail = async function(req, res){
    try{
        const email = req.params.email

        // use joi to check req validation
        const schema = Joi.object({
            email: Joi.string().email().required(),
        });
        const result = schema.validate(req.params);
        if (result.error) {
            console.log(result.error.details[0].message);
            res.status(400).send({error: result.error.details[0].message});
            return;
        }

        const response = await new Promise((resolve, reject) => {
            const query = `SELECT email, role FROM admins WHERE email = '${email}'`;
            mysqlDb.query(query, (err, results) => {
                if (err) reject(new Error(err.message));
                resolve(results);
            })
        })
        .then(data => {
            if (data.length == 0){
                res.status(400).send({error: 'admin does not exist'});
            }else {
                res.status(200).send({
                    email: data[0].email,
                    role: data[0].role
                });
            }
        })
        .catch(err => {res.status(400).send({error: "error occured"})});
    }catch(err){
        if (err){
            console.log(err);
            res.status(500).send({error: "error occured"});
        }
    }
}

//DELETE ADMIN
exports.deleteAdmins = async function(req, res){
    try {
        id = req.params.id;
        const response = await new Promise((resolve, reject) => {
           const query = "DELETE FROM admins WHERE id = " + mysqlDb.escape(id)
           mysqlDb.query(query, (err, results) => {
               if (err) reject(new Error(err.message));
               resolve(results);
           });
        })
        .then(data => data.affectedRows == 0 ? res.status(205).send('Item Does not exist') : res.status(200).send(' Item Deleted'))
        .catch(err => res.status(400).send('Error Occured'));
    } catch (error) {
        console.log(error);
    }
}