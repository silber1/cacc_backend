const mysqlDb = require('../dbService'),
    Joi = require('joi')

// CREATE TABLE
exports.createTable = async function(req, res) {
    try {
        let sql = `CREATE TABLE club (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, engname VARCHAR(255) NOT NULL, chiname VARCHAR(255) NOT NULL) CHARACTER SET = utf8;`
        const response = await new Promise((resolve, reject) => {
            mysqlDb.query(sql, (err, results) => {
                if (err) reject(new Error(err.message))
                resolve(results);
            });
        })
        .then(res.status(200).send('Club Table Created'))
        .catch(err => res.status(400).send('Error Occured'))
    } catch (error) {
        console.log(error)
    }
}

// CREATE CLUB
exports.createClub = async function(req, res) {
    console.log(req.body)
    const engname = req.body.engname,
            chiname = req.body.chiname;
    
    const schema = Joi.object({
        engname: Joi.string().required(),
        chiname: Joi.string().required()
    });
    const result = schema.validate(req.body);
    if (result.error) {
        res.status(400).send({error: result.error.details[0].message});
        return;
    }

    try {
        let cb = {
            "engname": engname,
            "chiname": chiname
        }
        const response = await new Promise((resolve, reject) => {
            let sql = 'INSERT INTO club SET ?';
            mysqlDb.query(sql, cb, (err, resluts) => {
                if (err) reject (new Error(err.message))
                resolve(resluts)
            });
        })
        .then(data => res.status(200).send("Club Created"))
        .catch(err => res.status(400).send(err));;
    } catch (err) {
        console.log(err)
    }
}

// DELETE CLUB
exports.deleteClub = async function(req, res) {
    try {
        const id = req.params.id;
        const response = await new Promise((resolve, reject) => {
            const sql = "DELETE FROM club WHERE id = " + mysqlDb.escape(id);
            mysqlDb.query(sql, (err, results) => {
                if (err) reject(new Error(err.message));
                resolve(results);
            });
        })
        .then(data => data.affectedRows == 0 ? res.status(205).send('Club Does not exist') : res.status(200).send(' Club Deleted'))
        .catch(err => res.status(400).send('Error Occured'));
    } catch (error) {
        console.log(error)
    }
}

// GET CLUB BY ID
exports.getClubById = async function(req, res) {
    try {
        const id = req.params.id;
        const response = await new Promise((resolve, reject) => {
            const sql = "SELECT * FROM club WHERE id = " + mysqlDb.escape(id);
            mysqlDb.query(sql, (err, results) => {
                if (err) reject(new Error(err.message));
                resolve(results)
            });
        })
        .then(data => res.json(data))
        .catch(err => res.status(500).send("ERROR OCCURED"));
    } catch (err) {
        console.log(err)
    }
}

// GET ALL CLUBS
exports.getAllClubs = async function(req, res) {
    try {
        const response = await new Promise((resolve, reject) => {
            const sql = "SELECT * FROM club";
            mysqlDb.query(sql, (err, results) => {
                if (err) reject(new Error(err.message));
                resolve(results)
            });
        })
        .then(data => res.json(data))
        .catch(err => res.status(500).send("ERROR OCCURED"));
    } catch(err) {
        console.log(err)
    }
}







